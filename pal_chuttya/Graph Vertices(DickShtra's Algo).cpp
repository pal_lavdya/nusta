//Graph Vertices
#include<iostream>
#define MAX 10
using namespace std;

class Graph
{
	int a[MAX][MAX];
	int n,src,dest;
	string adj[10];
	public:
		Graph()
		{
			n=0;
			dest=0;
			src=0;
		}

		void createAdj()
		{
			char ans;
			int wt;
			cout<<"Enter the number of vertices in the graph"<<endl;
			cin>>n;
			cout<<"Enter the name of "<<n<<" vertices, you want to give"<<endl;
			for(int i=0;i<n;i++)
			{
				cin>>adj[i];
			}
		 	for(int i=0;i<n;i++)
				for(int j=0;j<n;j++)
					a[i][j]=0;
			for(int i=0;i<n;i++)
			{
				for(int j=i+1;j<n;j++)
				{
					if(i!=j)
					{





						cout<<"Is there a edge between "<<adj[i]<<" and "<<adj[j]<<" : ";
						cin>>ans;
						if(ans=='y' || ans=='Y')
						{
							cout<<"Enter the weight of edge : ";
							cin>>wt;
							a[j][i]=a[i][j]=wt;
						}
					}
				}
			}
		}

		void display()
		{
			cout<<"The Adjacency Matrix is :"<<endl;
			for(int i=0;i<n;i++)
			{
				for(int j=0;j<n;j++)
				{
					cout<<"	"<<a[i][j];
				}
				cout<<endl;
			}
		}

		void DJ()
		{
			int curr=0;
			int curr_dist=0;
			int min=0, k=0;
			int newcost=0;
			int i=0;
			int visit[MAX],cost[MAX],prev[MAX];
			string source, destination;
			cout<<"From which vertex, you want to start"<<endl;
			cin>>source;
			src=getIndex(source);
			cout<<"Source : "<<src<<endl;
			cout<<"At which vertex, you want to end"<<endl;
			cin>>destination;
			dest=getIndex(destination);
			cout<<"Dest : "<<dest<<endl;
			for(int i=0;i<MAX;i++)
			{
				visit[i]=0;




				cost[i]=999;
				prev[i]=999;
			}
			visit[src]=1;
			curr=src;
			cost[src]=0;
			while(curr!=dest)
			{
				curr_dist=cost[curr];
				min=999;
				for(int i=0;i<n;i++)
				{
					if(visit[i]==0 && a[curr][i]!=0)
					{
						newcost=curr_dist + a[curr][i];
						if(newcost<cost[i])
						{
							cost[i]=newcost;
							prev[i]=curr;
						}
						if(cost[i]<min)
						{
							min=cost[i];
							k=i;
						}
					}
				}
				curr=k;
				visit[curr]=1;
			}
			k=0;
			int path[MAX];
			int dest1 = dest;
			while(src!=dest)
			{
				path[k]=prev[dest];
				dest=prev[dest];
				k++;
			}
			k--;
			while(k>=0)
			{
				cout<<"	"<<getLocation(path[k])<<"-->";
				k--;
			}
			cout<<"	"<<getLocation(dest1);
			cout<<endl<<"Minimum cost is "<<cost[dest1];
		}





		string getLocation(int i)
		{
			return adj[i];
		}

		int getIndex(string search)
		{
			for(int i=0;i<n;i++)
				if(adj[i]==search)
					return i;
		}
};

int main()
{
	Graph g;
	g.createAdj();
	g.display();
	g.DJ();
	return 0;
}
