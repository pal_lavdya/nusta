//DFS and BFS
#include <iostream>
using namespace std;

class graph
{
	friend class stack;
	int n;
	int adj[50][50];
	int chk[50];
public:
	graph();
	void create();
	void dfs();
	void bfs();
};

class stack
{
	int item[50];
		int top;

public:


	stack()
	{
		top=-1;
	}
	int pop()
	{
		int x;
		if(isEmpty())
		{
			cout<<"\n stack is empty";

		}
		else
		{
			x = item[top];
			top--;
		}
		return x;
	}
	int isEmpty()
	{ if(top==-1)
		return 1;
	else
		return 0;

	}
	int isFull()
	{
		if(top==50)
			return 1;
		else
			return 0;
	}
	void push(int x)
	{
		if(isFull())
			cout<<"\nStack is full";
		else
		{
			top++;
			item[top] = x;
		}
	}

};

class queue
{
	int item[50];
		int front,rear;

public:


	queue()
	{
		front=-1;
		rear = -1;
	}

	int pop()
	{
		int x;
		if(isEmpty())
		{
			cout<<"\n queue is empty";

		}
		else
		{
			x = item[front];
			front++;
		}
		return x;
	}
	int isEmpty()

	{
		if((front==-1)||(front>rear))
			return 1;
		else
			return 0;

	}
	int isFull()
	{
		if(front==50)
			return 1;
		else
			return 0;
	}
	void push(int x)
	{
		if(isFull())
			cout<<"\nqueue is full";
		else
		{
			if(front==-1)
			front++;
			item[++rear] = x;
		}
	}

};

graph :: graph()
{
	for (int i = 0; i < 50; i++)
		for(int j = 0; j < 50; j++)
			adj[i][j]=0;

	for (int i = 0; i<50; i++)
		chk[i]=0;
}

void graph :: create()
{
	int ch;
	cout<<"No. of Vertices in a graph ::  ";
	cin>>n;

	for (int i = 0; i< n-1; i++)
	 {
		for(int j = i+1; j < n; j++)
		{
			cout<<"does edge is present between "<< i <<" and " <<j<<" ?(1/0)";
			cin >> ch;

			if(ch==1)
				adj[i][j] = adj[j][i] = 1;
		}
	}
}


void graph :: dfs()
{
	stack s;
	int i;
	cout<<"\n--------------------DFS---------------------\n";
	cout<<"Enter starting vertex :: ";
	cin >> i;

	s.push(i);

	while(!s.isEmpty())
	{
		i = s.pop();

		if(chk[i]==1)
			continue;

		cout<<" "<<i;

		chk[i] = 1;

		for (int k = 0; k < n; k++)
		{
			if(adj[i][k] == 1 && chk[k] == 0)
				s.push(k);
		}

	}
	for (int i = 0; i<50; i++)
			chk[i]=0;
}

void graph :: bfs()
{
	queue s;
	int i;
	cout<<"\n--------------------BFS---------------------\n";
	cout<<"Enter starting vertex :: ";
	cin >> i;

	s.push(i);

	while(!s.isEmpty())
	{
		i = s.pop();

		if(chk[i]==1)
			continue;

		cout<<" "<<i;

		chk[i] = 1;

		for (int k = 0; k < n; k++)
		{
			if(adj[i][k] == 1 && chk[k] == 0)
				s.push(k);
		}

	}

	for (int i = 0; i<50; i++)
			chk[i]=0;
}


int main()
{
	graph g;
	g.create();
	g.dfs();

	g.bfs();
	return 0;
}
