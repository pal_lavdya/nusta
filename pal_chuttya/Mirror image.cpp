//Mirror image
#include <iostream>
#include<string.h>
using namespace std;
class node
{
 public:
 char data[20];
  class node  *left;
 class node * right;
};
class BST
{
public:
 node *root;
 node *temp;
 void create();
 void disp(node *);
 void insert(node * root,node *temp);
 void mirror();
 int height(node*);
 void printleafnodes(node*temp);
 void mirror1(node * pt);
};
void BST :: create()
{
 class node *temp;
 int ch;
 do
 {
  temp = new node;
  cout<<"\nEnter Data:";
  cin>>temp->data;
	temp->left = NULL;
  temp->right = NULL;
	if(root == NULL)
  {
   root = temp;
  }
  else
  {
   insert(root, temp);
  }
  cout<<"\nDo u want to add more (y=1/n=0):";
  cin>>ch;
 }
 while(ch == 1);
}
void BST ::  insert(node *root,node *temp)
{
 if(temp->data<root->data)
 {
  if(root->left == NULL)
   root->left = temp;
  else
   insert(root->left,temp);
 }
 else if(temp->data>root->data)
 { if(root->right == NULL)
   root->right = temp;
  else
   insert(root->right,temp);
 }
}
void BST:: disp(node * root)
{
 if( root != NULL)
 {
  disp(root->left);
  cout<<"\n Data :"<<root->data;
  disp(root->right);
 }
}
int BST :: height(node *pt)
{
int lt,rt;
if(pt!=NULL)
{
lt=height(pt->left);
rt=height(pt->right);
if(lt<rt)
return (rt+1);
else
return (lt+1);
}
}
void BST:: printleafnodes(node *root)
{
if(root!=NULL)
{
printleafnodes(root->left);
if(root->left==NULL && root->right==NULL)
cout<<root->data;
printleafnodes(root->right);
}
}
void BST :: mirror()
{
node *pt;
pt=root;
cout<<pt->data;
mirror1(root);
}
void BST :: mirror1(node *pt)
{
if(pt!=NULL)
{
mirror1(pt->left);
mirror1(pt->right);
temp=pt->left;
pt->left=pt->right;
pt->right=temp;
}
}
int main()
{
 int ch;
 int aa;
 BST b;
 b.root = NULL;
 do
 {
  cout<<"\nMenu\n1.Create\n2.Disp\n3.Height\n4.Leafnodes\n5.Mirror\nEnter Ur CH:";
  cin>>ch;

  switch(ch)
  {
case 1: b.create();
  break;
case 2: if(b.root == NULL)
  {
  cout<<"\nNo any Keyword";
  }
  else
  {
  b.disp(b.root);
  }
  break;
case 3: if(b.root == NULL)
 {
  cout<<"\nTree is Empty. First add elements then try again ";
 }
  else
 {
aa=b.height(b.root);
cout<<"height"<<aa;
      }
  break;
case 4:
  if(b.root == NULL)
  {
  cout<<"\nTree is Empty. First add elements then try again ";
 }
  else
  {
  b.printleafnodes(b.root);
  }
  break;
case 5:
  if(b.root == NULL)
  {
  cout<<"\nTree is Empty. First add elements then try again ";
  }
  else
  {
b.mirror();
b.disp(b.root);
  }
}
  }
 while(ch<=5);
 return 0;
}
